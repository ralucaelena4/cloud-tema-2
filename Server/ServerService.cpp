#include "ServerService.h"
#include <fstream>

::grpc::Status ServerService::ClientSign(::grpc::ServerContext* context, const::Date* date, ::Sign* sign)
{ 
    static std::array<HoroscopeMonth, 12> zodiacMonths;
    int daySt; std::string fmonth, lmonth;
    std::ifstream zodiacFile("ZodiacSigns.txt");
    for (HoroscopeMonth& month : zodiacMonths)
    {
        zodiacFile >> daySt;
        zodiacFile >> fmonth;
        zodiacFile >> lmonth;
        month.setPartionDay(daySt);
        month.setFirstSign(fmonth);
        month.setLastSign(lmonth);
    }
    
    std::string monthS = date->date().substr(0, 2);
    std::string dayS = date->date().substr(3, 4);
    
    int month = std::stoi(monthS);
    int day = std::stoi(dayS);

    if (day <= zodiacMonths[month - 1].getPartionDay())
        sign->set_sign(zodiacMonths[month - 1].getFirstSign());
    else sign->set_sign(zodiacMonths[month - 1].getLastSign());

    return ::grpc::Status::OK;
}

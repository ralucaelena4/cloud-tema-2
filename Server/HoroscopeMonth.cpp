#include "HoroscopeMonth.h"

std::istream& operator>>(std::istream& is, HoroscopeMonth& month)
{
	is >> month.partionDay;
	is >> month.firstSign;
	is >> month.lastSign;
	return is;
}

std::ostream& operator<<(std::ostream& os, const HoroscopeMonth& month)
{
	os << "Last day of the first sign in this month is " << month.partionDay << std::endl;
	os << "First sign is " << month.firstSign << std::endl;
	os << "Last sign is " << month.lastSign << std::endl;
	return os;
}

HoroscopeMonth::HoroscopeMonth()
{
	partionDay = 0;
	firstSign = "";
	lastSign = "";
}

void HoroscopeMonth::setPartionDay(const int& day)
{
	this->partionDay = day;
}

void HoroscopeMonth::setFirstSign(std::string sign)
{
	this->firstSign = sign;
}

void HoroscopeMonth::setLastSign(const std::string& sign)
{
	this->lastSign = sign;
}

int HoroscopeMonth::getPartionDay()
{
	return partionDay;
}

std::string HoroscopeMonth::getFirstSign()
{
	return firstSign;
}

std::string HoroscopeMonth::getLastSign()
{
	return lastSign;
}

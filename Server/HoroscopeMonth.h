#pragma once
#include <iostream>
#include <string>

class HoroscopeMonth
{
public:
    HoroscopeMonth();

    friend std::istream& operator >> (std::istream& is, HoroscopeMonth& month);
    friend std::ostream& operator << (std::ostream& os, const HoroscopeMonth& month);

    void setPartionDay(const int& day);
    void setFirstSign(std::string sign);
    void setLastSign(const std::string& sign);

    int getPartionDay();
    std::string getFirstSign();
    std::string getLastSign();
private:
    int partionDay;
    std::string firstSign;
    std::string lastSign;
};


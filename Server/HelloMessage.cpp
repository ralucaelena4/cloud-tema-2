
#include "HelloMessage.h"

::grpc::Status HelloMessage::SayHello(::grpc::ServerContext* context, const::ClientName* clientName, ::Reply* reply)
{
	reply->set_message("Hello " + clientName->name() + "!");
	std::cout << reply->message() << std::endl;
	return ::grpc::Status::OK;
}

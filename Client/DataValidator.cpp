#include "DataValidator.h"

std::ostream& operator<<(std::ostream& os, const DataValidator& date)
{
	os << date.month << "/" << date.day << "/" << date.year;
	return os;
}

std::istream& operator>>(std::istream& is, std::string& date)
{
	is >> date;
	return is;
}

bool DataValidator::isDate(const std::string& date)
{
	for (char index : date)
		if (!isdigit(index) && index != '/')
			return false;
	return true;
}

bool DataValidator::isLeapYear(const DataValidator& date)
{
	if (date.year % 4 != 0)
		return false;
	if (date.year % 100 != 0)
		return true;
	if (date.year % 400 != 0)
		return false;
	return true;
}

DataValidator DataValidator::transformIntoDate(const std::string& inputDate) // 02.12.2010 month/day/year
{
	std::string dayS, monthS, yearS;
	monthS = inputDate.substr(0, 2);
	dayS = inputDate.substr(3, 4);
	yearS = inputDate.substr(6, 9);

	DataValidator date;
	date.month = std::stoi(monthS);
	date.day = std::stoi(dayS);
	date.year = std::stoi(yearS);

	return date;
}

bool DataValidator::validateDate(const std::string& dateInput)
{
	if (!isDate(dateInput))
	{
	std::cout << "Invalid input! " << std::endl;
	return false;
	}

	DataValidator date = transformIntoDate(dateInput);

	if ((date.year < 999) || (date.year > 10000))
	{
		std::cout << "Year not available!" << std::endl;
		return false;
	}
	if ((date.month < 1) || (date.month > 12))
	{
		std::cout << "Month does not exist!" << std::endl;
		return false;
	}
	if ((date.day < 1) || (date.day > 31))
	{
		std::cout << "Day does not exist!" << std::endl;
		return false;
	}
	if ((date.month == 1 || date.month == 3 || date.month == 5 || date.month == 7 || date.month == 8 || date.month == 10 || date.month == 12) && date.day > 31)
	{
		std::cout << "This month only has 31 days!" << std::endl;
		return false;
	}
	if ((date.month == 4 || date.month == 6 || date.month == 9 || date.month == 11) && date.day > 30)
	{
		std::cout << "This month only has 30 days!" << std::endl;
		return false;
	}
	if (isLeapYear(date) && date.month == 2 && date.day > 29)
	{
		std::cout << "It's a leap year, february has 29 days!" << std::endl;
		return false;
	}
	if (!isLeapYear(date) && date.month == 2 && date.day > 28)
	{
		std::cout << "It's not a leap year, february has 28 days!" << std::endl;
		return false;
	}
	return true;
}

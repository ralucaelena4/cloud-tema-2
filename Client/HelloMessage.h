#pragma once
#include <Message.grpc.pb.h>

class HelloMessage final : public Greet::Service
{
public:
	HelloMessage() {};
	::grpc::Status SayHello(::grpc::ServerContext* context, const::ClientName* clientName, ::Reply* reply) override;
};
